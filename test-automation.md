# Assessment 2/2:


## Test Automation

The second part of the assessment is about examining your technical abillities with the tools we use in our team. 

You may take as long as you want but a general guideline should be that you could finish both these assignments (easily) within 1 working day.

* *Don't add any documentation. The code should be readable and document itself.*

---

## Create an automated test

The main goal of your work is to design and automate testcases. In this part of the assignment you'll show you're familiarity with test-automation.

### Implement a test

*preferably using:*

- Build tools: Maven (or Gradle)
- BDD: JBehave (or Cucumber)
- Testing-Framework: Serenity
- Api-Testing: Rest-Assured

---

Pre-requisites:

- A github account (not necessarily your own)
- optionally a github api key to overcome api limitations

The test should contain:

### An API part:

1. Make a request to: `https://api.github.com/users/{username}` 
2. Assure that the expected StatusCode is returned.
3. From the response retrieve the value of `repos_url`
4. Use the value from step 3 to retrieve the number of repos for this user.

### A GUI part:

1. Open GitHub.com in chrome
2. search for the above user and open the users github page
3. get the number of repos for this user from the browser

### Assert the API data with the GUI data

1. Assert that the number of repos you got from the API part is equal to the number of repos you got from the GUI part.

---

## Create a CI/CD pipeline for the test

Part of your work will be maintaining the testing CI/CD with which we run our tests. We have setup our testing pipeline in GitLab and run our tests every night. 

FYI: GitLab explains in its docs how to setup a Pipeline and create a schedule for it.

In this part of the assignment it's your task to create a CI/CD pipeline in GitLab in which you'll configure a pipeline to run the above test. 

### Prerequisites:

- the test you created above
- a gitlab account

The pipeline should 

- be configured in the .gitlab-ci.yml
- contain a job in which you run the above test
- store the test-report as artifacts
- optionally: publish the test-report with the `pages`  job

