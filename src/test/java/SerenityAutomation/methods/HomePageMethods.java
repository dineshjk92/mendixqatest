package SerenityAutomation.methods;

import SerenityAutomation.pages.HomePage;
import SerenityAutomation.pages.LoginPage;

public class HomePageMethods extends BaseMethods {

    public static void waitForHomePageLoad() {
        waitUntilVisible(HomePage.signinButton);
    }

    public static void searchUser(String username) {
        clearAndType(HomePage.userSearchField, username);
        waitAndClick(HomePage.userSuggestionList);
    }

    /*****************************************************/
    public void loginUsingCredentials(String username, String password) {
        displayInformationAboutPageLogin();
        inputUsername(username);
        inputPassword(password);
    }

    private void displayInformationAboutPageLogin() {
        String actualPageTitle = getDriver().getTitle();
        String actualPageUrl = getDriver().getCurrentUrl();
        System.out.println("Current page name is: " +actualPageTitle);
        System.out.println("Current page URL is: " +actualPageUrl);
        System.out.println("Login in using provided credentials...");
    }

    private void inputUsername(String username) {
        LoginPage.usernameTextbox.waitUntilVisible();
        LoginPage.usernameTextbox.type(username);
    }

    private void inputPassword(String password) {
        LoginPage.passwordTextbox.waitUntilVisible();
        LoginPage.passwordTextbox.typeAndEnter(password);
    }
}