package SerenityAutomation.methods;

import SerenityAutomation.pages.LoginPage;
import SerenityAutomation.pages.SearchResultPage;

public class SearchResultPageMethods extends BaseMethods {

    public static void waitForSearchResultPageLoad() {
        waitUntilVisible(SearchResultPage.userMenuItem);
    }

    public static boolean isUserMenuDisplayed() {
        return SearchResultPage.userMenuItem.isVisible();
    }

    public static int getUserCount() {
        return Integer
                .parseInt(SearchResultPage.userCount.getText());
    }
    public static void navigateToUserProfilePage() {
        waitAndClick(SearchResultPage.userMenuItem);
        waitAndClick(SearchResultPage.userProfileLink);
    }

    /*****************************************************/
    public void loginUsingCredentials(String username, String password) {
        displayInformationAboutPageLogin();
        inputUsername(username);
        inputPassword(password);
    }

    private void displayInformationAboutPageLogin() {
        String actualPageTitle = getDriver().getTitle();
        String actualPageUrl = getDriver().getCurrentUrl();
        System.out.println("Current page name is: " +actualPageTitle);
        System.out.println("Current page URL is: " +actualPageUrl);
        System.out.println("Login in using provided credentials...");
    }

    private void inputUsername(String username) {
        LoginPage.usernameTextbox.waitUntilVisible();
        LoginPage.usernameTextbox.type(username);
    }

    private void inputPassword(String password) {
        LoginPage.passwordTextbox.waitUntilVisible();
        LoginPage.passwordTextbox.typeAndEnter(password);
    }
}