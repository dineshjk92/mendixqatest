package SerenityAutomation.methods;

import SerenityAutomation.pages.ProfilePage;

public class ProfilePageMethods extends BaseMethods {

    public static void waitForProfilePageLoad() {
        waitUntilVisible(ProfilePage.userNameLabel);
    }

    public static void clickOnRepositoriesTab() {
        waitAndClick(ProfilePage.repositoryTab);
    }

    public static int getRepositoryCount() {
        return Integer
                .parseInt(ProfilePage.repositoryCountText.getText());
    }
}