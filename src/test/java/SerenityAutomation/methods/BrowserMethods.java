package SerenityAutomation.methods;

import net.serenitybdd.core.Serenity;
import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.util.EnvironmentVariables;
import net.thucydides.core.util.SystemEnvironmentVariables;
import org.openqa.selenium.WebDriver;

public class BrowserMethods extends PageObject {

    public static EnvironmentVariables envVariables = SystemEnvironmentVariables.createEnvironmentVariables();

    public BrowserMethods(WebDriver driver) {
        super(driver);
    }

    public void closeBrowser () {
        getDriver().close();
    }

    public void closeBrowserAndDeleteCookies () {
        getDriver().manage().deleteAllCookies();
        getDriver().close();
    }

    public void openBrowserWithProvidedPage(String pageUrl) {
        getDriver().navigate().to(pageUrl);
        Serenity.takeScreenshot();
    }

    public void initBrowser() {
        String os = System.getProperty("os.name");

        if(os.contains("Windows 10")) {
            //String driverPath = envVariables.getProperty("drivers.windows.webdriver.chrome.driver");
            //envVariables.setProperty("webdriver.chrome.driver", driverPath);
        }
    }
}