package SerenityAutomation.definitions;

import SerenityAutomation.stepsDescriptions.APISteps;
import SerenityAutomation.stepsDescriptions.BrowserSteps;
import SerenityAutomation.stepsDescriptions.GetRepoDetailsSteps;
import SerenityAutomation.stepsDescriptions.SearchUserSteps;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.response.ResponseOptions;
import io.restassured.specification.RequestSpecification;
import net.thucydides.core.annotations.Steps;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Named;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class GithubRepoDefinitions {

    @Steps
    private APISteps apiSteps;

    @Steps
    private BrowserSteps browserSteps;

    @Steps
    private SearchUserSteps searchUserSteps;

    @Steps
    private GetRepoDetailsSteps getRepoDetailsSteps;

    private static Logger logger = LogManager.getLogger(GithubRepoDefinitions.class);

    RequestSpecification reqSpec;
    ResponseOptions<Response> response;
    String repo_url;


    @Given("I call the github api to fetch user details")
    public void givenICallTheGithubApiToFetchUserDetails(@Named("apibaseurl") String apibaseurl, @Named ("apikey") String apikey, @Named ("username") String username) {
        reqSpec = apiSteps.setupRequestSpec(apibaseurl, apikey);

        response = RestAssured.given()
                .spec(reqSpec)
                .contentType("application/json")
                .when()
                .get("users/{username}", username);

        logger.info("Response");
        response.getBody().prettyPrint();
    }

    @Then("The response status code should be $integer1")
    public void thenTheResponseStatusCodeShouldBe(int statusCode) {
        assertThat("Status code is not as expected", response.getStatusCode(), equalTo(statusCode));
    }

    @When("I fetch the value of $repo_param from the response")
    public void whenIFetchTheValueOfReposurlFromTheResponse(String repo_param) {
        logger.info("Fetching repo_url");
        repo_url = response.getBody().jsonPath().get(repo_param);
    }

    @When("I call the github api to fetch repo details")
    public void whenICallTheGithubApiToFetchRepoDetails(@Named("apibaseurl") String apibaseurl, @Named ("apikey") String apikey) {
        reqSpec = apiSteps.setupRequestSpec(apibaseurl, apikey);

        response = RestAssured.given()
                .spec(reqSpec)
                .contentType("application/json")
                .when()
                .get(repo_url);

        logger.info("Response");
        response.getBody().prettyPrint();
    }


    @When("I visit the github webpage")
    public void whenIVisitTheGithubWebpage(@Named("pagebaseurl") String pagebaseurl) {
        logger.info("Visit the github page");
        browserSteps.openStoryDefinedPage(pagebaseurl);
    }

    @When("I search for the user")
    public void whenISearchForTheUser(@Named ("username") String username) {
        logger.info("Search for the user");
        searchUserSteps.searchForUser("user:" + username);
    }

    @Then("The user menu details should be displayed")
    public void thenTheUserDetailsShouldBeDisplayed() {
        logger.info("Verify usermenu is displayed");
        searchUserSteps.verifySearchResult();
    }

    @When("I navigate to the user profile page")
    public void whenINavigateToTheUserProfilePage(@Named("pagebaseurl") String pagebaseurl, @Named ("username") String username) {
        logger.info("Navigating to profile page");
        searchUserSteps.navigateToProfilePage();
        if(!searchUserSteps.isUserMenuDisplayed()) {
            logger.info("Navigating with direct url");
            browserSteps.openStoryDefinedPage(pagebaseurl+username);
        }
    }

    @When("I click on Repositories tab")
    public void whenIClickOnRepositoriesTab() {
        logger.info("Navigating to repositories page");
        getRepoDetailsSteps.navigateToRepoPage();
    }

    @Then("The number of repos from the webpage should match with that of from API")
    public void thenTheNumberOfReposFromTheWebpageShouldMatchWithThatOfFromApi() {
        logger.info("Verify repo count from UI and API");
        assertThat("The number of repos from the webpage doesn't match with that of from API", response.getBody().jsonPath().getList("$").size(), equalTo(getRepoDetailsSteps.getRepositoriesCount()));
    }
}
