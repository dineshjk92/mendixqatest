package SerenityAutomation.stepsDescriptions;

import SerenityAutomation.methods.ProfilePageMethods;
import SerenityAutomation.pages.ProfilePage;
import net.thucydides.core.annotations.Step;

public class GetRepoDetailsSteps {

    ProfilePage profilePage;

    @Step
    public void navigateToRepoPage() {
        ProfilePageMethods.clickOnRepositoriesTab();
    }

    @Step
    public int getRepositoriesCount() {
        return ProfilePageMethods.getRepositoryCount();
    }

}