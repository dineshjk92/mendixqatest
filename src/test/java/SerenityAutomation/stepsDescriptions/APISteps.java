package SerenityAutomation.stepsDescriptions;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.specification.RequestSpecification;
import net.thucydides.core.annotations.Step;

public class APISteps {

    @Step
    public RequestSpecification setupRequestSpec(String baseUrl, String... accessToken) {
        if(accessToken != null && accessToken.length > 0) {
            return new RequestSpecBuilder()
                    .setBaseUri(baseUrl)
                    .addHeader("Authorization", "token " + accessToken[0].trim())
                    .build();
        } else {
            return new RequestSpecBuilder()
                    .setBaseUri(baseUrl)
                    .build();
        }

    }
}
