package SerenityAutomation.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class ProfilePage extends PageObject {

    @FindBy(css = "span.vcard-username")
    public static WebElementFacade userNameLabel;

    @FindBy(css = "div.width-full a.UnderlineNav-item[href*='repositories']")
    public static WebElementFacade repositoryTab;

    @FindBy(css = "div.width-full a.UnderlineNav-item[href*='repositories'] span")
    public static WebElementFacade repositoryCountText;
}
