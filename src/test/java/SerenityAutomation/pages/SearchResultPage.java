package SerenityAutomation.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class SearchResultPage extends PageObject {

    @FindBy(xpath = "//a[@class='menu-item' and contains(@href, 'type=users')]")
    public static WebElementFacade userMenuItem;

    @FindBy(xpath = "//a[@class='menu-item' and contains(@href, 'type=users')]//span")
    public static WebElementFacade userCount;

    @FindBy(css = "div#user_search_results a.color-text-secondary")
    public static WebElementFacade userProfileLink;

}
